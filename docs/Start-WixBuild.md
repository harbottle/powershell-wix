---
external help file: powershell-wix-help.xml
Module Name: powershell-wix
online version: https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Start-WixBuild.md
schema: 2.0.0
---

# Start-WixBuild

## SYNOPSIS
Converts a PowerShell module folder to installable MSI package files.

## SYNTAX

```
Start-WixBuild [[-Path] <String>] [-Exclude <String[]>] [-OutputFolder <String>] [-LicenseFile <String>]
 [-IconFile <String>] [-BannerFile <String>] [-DialogFile <String>] [-ProductShortName <String>]
 [-ProductName <String>] [-ProductVersion <String>] [-Manufacturer <String>] [-HelpLink <String>]
 [-AboutLink <String>] [-UpgradeCodeX86 <String>] [-UpgradeCodeX64 <String>] [-Increment <Int32>] [-NoX86]
 [-NoX64] [<CommonParameters>]
```

## DESCRIPTION
This function uses the WiX Toolset to convert a directory containing a
PowerShell module to an installable Microsoft Installer package file (MSI). By
default 32bit and 64bit MSI files are generated. The target install directories
for the MSI files are set to:

- C:\Program Files\WindowsPowerShell\Modules (64bit MSI on 64bit Windows and 32bit MSI on 32bit Windows).
- C:\Program Files (x86)\WindowsPowerShell\Modules (32bit MSI on 64bit Windows).

See the each function parameter for options.

## EXAMPLES

### Example 1
```
PS C:\> Start-WixBuild
```

Convert the current directory, containing a PowerShell module, into installable
MSI package files.

### Example 2
```
PS C:\> Start-WixBuild -Path 'C:\users\myuser\mymodules\awesomemodule'
```

Convert a PowerShell module in 'C:\users\myuser\mymodules\awesomemodule' into
installable MSI package files.

## PARAMETERS

### -AboutLink
URL for more information.  The URL will be displayed in 'Add/Remove Programs'
for your package.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -AboutLink -Path $Path).AboutLink
Accept pipeline input: False
Accept wildcard characters: False
```

### -BannerFile
The path to a bitmap file (.bmp). The image will be displayed at the top of
dialogue screens for your package. Defaults to 'dialog.bmp' in the
current directory. If this can't be found an included PowerShell dialog image
is used.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: $Path\banner.bmp
Accept pipeline input: False
Accept wildcard characters: False
```

### -DialogFile
The path to a bitmap file (.bmp). The image will be displayed on the first
installer dialogue screen for your package. Defaults to 'dialog.bmp' in the
current directory. If this can't be found an included PowerShell dialog image
is used.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: $Path\dialog.bmp
Accept pipeline input: False
Accept wildcard characters: False
```

### -Exclude
An array of file and folder patterns to exclude from the generated MSI package
files. Defaults to @('.git','.gitignore','\*.msi')

```yaml
Type: String[]
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: @('.git','.gitignore','*.msi')
Accept pipeline input: False
Accept wildcard characters: False
```

### -HelpLink
URL for more help. The URL will be displayed in 'Add/Remove Programs' for your
package.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -HelpLink -Path $Path).HelpLink
Accept pipeline input: False
Accept wildcard characters: False
```

### -IconFile
The path to an icon file (.ico). The icon will be displayed in
'Add/Remove Programs' for your package. Defaults to 'icon.ico' in the current
directory. If this can't be found an included PowerShell icon is used.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: $Path\icon.ico
Accept pipeline input: False
Accept wildcard characters: False
```

### -Increment
The part of the version number to increment.  To prevent a version number
increment, set this to 0. Defaults to 3. 

```yaml
Type: Int32
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: 3
Accept pipeline input: False
Accept wildcard characters: False
```

### -LicenseFile
The Rich Text File (.rtf) to use as a licence during installation of your
package.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: $Path\license.rtf
Accept pipeline input: False
Accept wildcard characters: False
```

### -Manufacturer
The package manufacturer

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -Manufacturer -Path $Path).Manufacturer
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoX64
Do not build x64 package.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -NoX86
Do not build x86 package.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -OutputFolder
The path to the folder to out the MSI package files. Defaults to current
directory.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-Location).Path
Accept pipeline input: False
Accept wildcard characters: False
```

### -Path
The path to the folder containing the PowerShell module to be converted.
Defaults to current directory.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: (Get-Location).Path
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductName
Product name

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -ProductName -Path $Path).ProductName
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductShortName
Short product name

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -ProductShortName -Path $Path).ProductShortName
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductVersion
{{Fill ProductVersion Description}}

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -ProductVersion -Path $Path).ProductVersion
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpgradeCodeX64
X64 upgrade code

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -UpgradeCodeX64 -Path $Path).UpgradeCodeX64
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpgradeCodeX86
X86 upgrade code

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-WiXLocalConfig -UpgradeCodeX86 -Path $Path).UpgradeCodeX86
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None

## OUTPUTS

### None

## NOTES
NAME:  Add-PulpRpm

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
