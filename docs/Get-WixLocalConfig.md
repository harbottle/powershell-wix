---
external help file: powershell-wix-help.xml
Module Name: powershell-wix
online version: https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Get-WixLocalConfig.md
schema: 2.0.0
---

# Get-WixLocalConfig

## SYNOPSIS
Returns the local WiX configuration.

## SYNTAX

```
Get-WixLocalConfig [[-Path] <String>] [-ProductShortName] [-ProductName] [-ProductVersion] [-Manufacturer]
 [-HelpLink] [-AboutLink] [-UpgradeCodeX86] [-UpgradeCodeX64] [<CommonParameters>]
```

## DESCRIPTION
This function returns an object representing the local WiX configuration.
Configuration is returned from a JSON formatted config file (default location:
'..wix.json'). Sensible default values are returned if they are not contained
in this file. Upgrade codes are generated and stored if they do not exist.

Individual configuration values can also be selected. If no configuration values
are selected, all values are returned.

## EXAMPLES

### Example 1
```
PS C:\> Get-WixLocalConfig
```

Get local WiX configuration from '.wix.json'.

### Example 2
```
PS C:\> (Get-WixLocalConfig -ProductName).ProductName
```

Get 'ProductName' as a string from '.wix.json'.

## PARAMETERS

### -AboutLink
Include "AboutLink" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -HelpLink
Include "HelpLink" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Manufacturer
Include "Manufacturer" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Path
Folder to look for a '.wix' file.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: (Get-Location).Path
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductName
Include "ProductName" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductShortName
Include "ProductShortName" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductVersion
Include "ProductVersion" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpgradeCodeX64
Include "UpgradeCodeX64" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpgradeCodeX86
Include "UpgradeCodeX86" in returned object.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### None

## OUTPUTS

### System.Management.Automation.PSCustomObject
A custom object representing the local WiX configuration.

## NOTES
NAME:  Add-PulpRpm

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
