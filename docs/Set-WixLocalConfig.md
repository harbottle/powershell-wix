---
external help file: powershell-wix-help.xml
Module Name: powershell-wix
online version: https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Set-WixLocalConfig.md
schema: 2.0.0
---

# Set-WixLocalConfig

## SYNOPSIS
Sets local WiX configuration.

## SYNTAX

### Object
```
Set-WixLocalConfig [-Path <String>] [-Replace] [-Settings] <Object> [<CommonParameters>]
```

### Strings
```
Set-WixLocalConfig [-Path <String>] [-Replace] [-ProductShortName <String>] [-ProductName <String>]
 [-ProductVersion <String>] [-Manufacturer <String>] [-HelpLink <String>] [-AboutLink <String>]
 [-UpgradeCodeX86 <String>] [-UpgradeCodeX64 <String>] [<CommonParameters>]
```

## DESCRIPTION
This function accepts an object representing configuration settings or
individual configuration settings and writes them to a JSON formatted file
(default location: '.wix.json'). It returns the new configuration.

## EXAMPLES

### Example 1
```
PS C:\> Set-WixLocalConfig -ProductName "My Awesome PowerShell Module"
```

Sets the 'ProductName' setting in default config file '.wix.json'.

## PARAMETERS

### -AboutLink
Set "AboutLink" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -HelpLink
Set "HelpLink" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Manufacturer
Set "Manufacturer" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Path
Folder to look for a '.wix' file.

```yaml
Type: String
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: (Get-Location).Path
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductName
Set "ProductName" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductShortName
Set "ProductShortName" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -ProductVersion
Set "ProductVersion" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -Replace
Replace all existing configuration with new settings.

```yaml
Type: SwitchParameter
Parameter Sets: (All)
Aliases:

Required: False
Position: Named
Default value: False
Accept pipeline input: False
Accept wildcard characters: False
```

### -Settings
Object containing all settings to be set.

```yaml
Type: Object
Parameter Sets: Object
Aliases:

Required: True
Position: 0
Default value: None
Accept pipeline input: True (ByValue)
Accept wildcard characters: False
```

### -UpgradeCodeX64
Set "UpgradeCodeX64" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### -UpgradeCodeX86
Set "UpgradeCodeX86" value.

```yaml
Type: String
Parameter Sets: Strings
Aliases:

Required: False
Position: Named
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

### CommonParameters
This cmdlet supports the common parameters: -Debug, -ErrorAction, -ErrorVariable, -InformationAction, -InformationVariable, -OutVariable, -OutBuffer, -PipelineVariable, -Verbose, -WarningAction, and -WarningVariable. For more information, see about_CommonParameters (http://go.microsoft.com/fwlink/?LinkID=113216).

## INPUTS

### System.Management.Automation.PSCustomObject
A custom object representing the local WiX configuration. Use
Get-WixLocalConfig to see an example of the object format.

## OUTPUTS

### System.Management.Automation.PSCustomObject
A custom object representing the new local WiX configuration.

## NOTES
NAME:  Add-PulpRpm

AUTHOR: Richard Grainger \<grainger@gmail.com\>

## RELATED LINKS
