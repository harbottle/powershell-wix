# powershell-wix

![logo](logo128.png)

**Release your PowerShell modules the professional way!**

A Windows PowerShell module to easily package your own PowerShell modules into
MSI files.

## Prerequisites
  - Windows PowerShell 3 or above
  - [WiX Toolset](http://wixtoolset.org/releases/)

## Install
Either:
 - (Per-computer, admin elevation required)
   [download](https://gitlab.com/harbottle/powershell-wix/tags) and install the
   latest powershell-wix MSI package, or:
 - (Per-user, admin elevation not required) install with
   [PowerShellGet](https://msdn.microsoft.com/powershell/reference/5.1/PowerShellGet/PowerShellGet)
   from the [PowerShell Gallery](https://www.powershellgallery.com/):
   
   ```powershell
   Install-Module -Name powershell-wix -Scope CurrentUser
   ```

## Usage
After installing the MSI file, the module should autoload the next time you
start PowerShell.  To enable the module manually, type the following:

````powershell
Import-Module powershell-wix
````

To convert your PowerShell module into MSI files:

````powershell
cd My_Awesome_Module
Set-WixLocalConfig -ProductName "My Awesome PowerShell Module" -Manufacturer "John Smith"
Start-WixBuild
````

That's it! 64 bit and a 32 bit MSI package files containing your module will be
generated in your module's directory .  When your users install the package, the
module will be installed in their standard PowerShell module path, ready for
use.

New versions of your module package will cleanly uninstall any older versions on
the user's system.

## Config details
MSI config for your module is stored in `.wix.json` in the root of your module.

Here is the the `.wix.json` file for powershell-wix as an example:

```json
{
    "ProductShortName":  "powershell-wix",
    "UpgradeCodeX86":  "656F16A6-1E96-210D-3EC4-606FCE5F60C1",
    "UpgradeCodeX64":  "37E5EDE9-748C-1485-0FB2-7FDB9F2769BF",
    "Manufacturer":  "grainger@gmail.com",
    "ProductName":  "Windows PowerShell module for WiX",
    "AboutLink":  "https://gitlab.com/harbottle/powershell-wix",
    "HelpLink":  "https://gitlab.com/harbottle/powershell-wix",
    "ProductVersion":  "1.0.8"
}
```

A `wix.json` file containing sensible default values will be generated the first
time you run any of the powershell-wix cmdlets. Your `.wix.json` keys can be
read using
[Get-WixLocalConfig](https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Get-WixLocalConfig.md)
and changed using
[Set-WixLocalConfig](https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Set-WixLocalConfig.md).
Your `.wix.json` file should be checked into your source code repo with your
other module files.

### Upgrade Codes
The keys `UpgradeCodeX86` and `UpgradeCodeX64` are important because they
represent GUIDs for your packages.  They allow Microsoft Installer to identify
older versions of your package and to upgrade them cleanly. Random values will
be generated for these keys and stored in `.wix.json` the first time you run any
of the powershell-wix cmdlets, so you do not need to specify them yourself.
However, you should **not** change these values unless you want subsequent
versions of your packages to be treated as a different package by Windows
Installer. This is one reason it is important to keep the `.wix.json` file with
your module code (e.g. checked-in to your source code repo) so that these
persistent codes can always be read when building new versions of your packages.

### Product Version
When using `Start-WixBuild` the version number will be read from `.wix.json`.
By default, the tertiary component of the version number will then be
incremented in `wix.json` ready for the next build. If you want the primary
component to be incremented, specify `-Increment 1`.  If you want the secondary
component to be incremented, specify `-Increment 2`.  If you do not want the
version number to be incremented in `.wix.json`, specify `-Increment 0`.

## Installer Resources

You can optionally customize the following resources in your generated MSI
files.  Place customized versions in the root of your module before building:

- `license.rtf` (the end user licence agreement used in installer dialog)
- `icon.ico` (the icon for your package, used in "Add/Remove Programs")
- `banner.bmp` (used in installer dialog)
- `dialog.bmp` (used in installer dialog)

It is probably best to at least provide `license.rtf`, as the default is just
*Lorem ipsum dolor* placeholder text.

## Functions
- [Start-WixBuild](https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Start-WixBuild.md)
- [Get-WixLocalConfig](https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Get-WixLocalConfig.md)
- [Set-WixLocalConfig](https://gitlab.com/harbottle/powershell-wix/blob/master/docs/Set-WixLocalConfig.md)

## Getting help
Use `Get-Help` cmdlet:

````powershell
Get-Help Start-WixBuild -Online
````

## License, copyright and acknowledgements
**powershell-wix**: Copyright (c) 2016 Richard Grainger,
[MIT License](https://opensource.org/licenses/MIT)

**WiX Toolset**: Copyright (c) 2016 Outercurve Foundation,
[Microsoft Reciprocal License (MS-RL)](http://opensource.org/licenses/ms-rl)

Inspired by [this](http://viziblr.com/news/2012/8/17/how-to-easily-create-an-msi-to-install-your-powershell-modul.html).
