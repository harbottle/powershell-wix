FROM harbottle/wix
LABEL maintainer="harbottle <grainger@gmail.com>"

RUN yum -y -q install microsoft-release
RUN yum -y -q install powershell
RUN pwsh -c "& {Install-Module powershell-wix -Confirm:\$false -SkipPublisherCheck -Force -Scope AllUsers}"
RUN pwsh -c "& {Import-Module powershell-wix}"
CMD [ "pwsh" ]
